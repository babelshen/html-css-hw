const menuButton = document.querySelector('.header-mobile__nav');
const menu = document.querySelector('.header__nav');
const body = document.body;
  
menuButton.addEventListener('click', () => {
    menu.classList.toggle('active');
    body.classList.toggle('menu-open'); 
});

window.addEventListener('click', e => {
    if (document.querySelector('.header__nav.active')?.contains(e.target) && 
        !e.target.closest('.header__nav-list li'))  {
      menu.classList.remove('active');
      body.classList.remove('menu-open');
    }
});

const buttonSocialMedia = document.querySelector('.header-mobile__social-media');
const menuSocialMedia = document.querySelector('.header__social-media');

buttonSocialMedia.addEventListener('click', () => {
    menuSocialMedia.classList.toggle('active');
    body.classList.toggle('menu-open'); 
});

window.addEventListener('click', e => {
    if (document.querySelector('.header__social-media.active')?.contains(e.target) && 
        !e.target.closest('.header__social-media.active li')) {
        menuSocialMedia.classList.remove('active');
        body.classList.remove('menu-open');
    }
});

const categoryButton = document.querySelector('.popular-topics__category-button');
const categoryMenu = document.querySelector('.popular-topics__category');

categoryButton.addEventListener('click', () => {
    categoryMenu.classList.toggle('active');
});

window.addEventListener('click', e => {
    if (categoryMenu.classList.contains('active') &&
    !categoryMenu.contains(e.target) &&
    !categoryButton.contains(e.target)) {
            categoryMenu.classList.remove('active');
    }
});